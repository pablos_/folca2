package com.folcademy.clinica.Model.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@Data
@Table(name = "paciente")
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;

    @Column(name = "dni", columnDefinition = "VARCHAR")
    public String dni;

    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    public String nombre;

    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    public String apellido;

    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    public String telefono;


    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o== null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Paciente paciente= (Paciente) o;

        return Objects.equals(idpaciente, paciente.idpaciente);
    }


}
