package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<TurnoDto>> findAll(){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos()
                );
    }

    @PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{idTurno}")
    public ResponseEntity<TurnoDto> findOneByID(@PathVariable(name = "idTurno") int idTurno){
        return  ResponseEntity.ok()
                .body(
                        turnoService.findTurnoByID(idTurno)
                );
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> addNewTurno(@RequestBody @Validated TurnoDto turnoDTO){
        return ResponseEntity.ok()
                .body(
                        turnoService.addNewTurno(turnoDTO)
                );
    }







}
