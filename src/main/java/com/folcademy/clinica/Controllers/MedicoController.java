package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {

    private final MedicoService medicoService;
    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<MedicoDto>> findAll() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }
    /*
    @GetMapping(value = "/{idmedico}")
    public ResponseEntity<List<Medico>> findAll(@PathVariable(name = "idmedico") Integer idmedico) {
        return ResponseEntity
                .ok()
                .body(
                        medicoService.findMedicoById(idmedico))
                ;
    }*/
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name= "idMedico") int id){
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    @PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto dto){
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    @PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name= "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto){

        return ResponseEntity.ok(medicoService.editar(id, dto));
    }

    @PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name= "idMedico") int id){
        return ResponseEntity.ok(medicoService.eliminar(id));
    }
}
