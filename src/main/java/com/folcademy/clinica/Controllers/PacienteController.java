package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    @GetMapping(value = "")
    public ResponseEntity<List<PacienteDto>> findAll() {
        return ResponseEntity.ok(
                        pacienteService.findAllPacientes())
                ;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PacienteDto> findAll(@PathVariable(name = "id") int id) {
        return ResponseEntity
                .ok(pacienteService.findPacienteById(id))
                ;
    }

    @PostMapping("")
    public ResponseEntity<PacienteDto> addPaciente(@RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.addPaciente(dto));
    }

    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> editPaciente(@PathVariable(name= "idPaciente") int id,
                                                  @RequestBody PacienteDto dto){
        return ResponseEntity.ok(pacienteService.editPaciente(id, dto));
    }

    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> deletePaciente(@PathVariable(name= "idMedico") int id){
        return ResponseEntity.ok(pacienteService.deletePaciente(id));
    }

}
