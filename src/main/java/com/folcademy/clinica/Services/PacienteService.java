package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Exceptions.BadRequestException;
import com.folcademy.clinica.Model.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService {

    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }


    public List<PacienteDto> findAllPacientes() {
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
        //return (List<Paciente>) pacienteRepository.findAll();
    }


    public PacienteDto findPacienteById(Integer id) {
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe Paciente con ese ID");

        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    };

    public PacienteDto addPaciente(PacienteDto entity){
        if(entity.getApellido().isEmpty() || entity.getApellido() == null || entity.getNombre().isEmpty() || entity.getNombre() == null){
            throw new ValidationException("Apellido y/o Nombre no pueden ser vacio");
        }

        entity.setIdpaciente(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    };

    public PacienteDto editPaciente(Integer idPaciente, PacienteDto dto){
        if(!pacienteRepository.existsById(idPaciente))
            throw new BadRequestException("Error Editar Paciente. No se encontro paciente con ese ID");

        dto.setIdpaciente(idPaciente);
        return pacienteMapper.entityToDto(
                pacienteRepository.save(pacienteMapper.dtoToEntity(dto)
                )
        );
    }

    public boolean deletePaciente(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("Error Eliminacion. No existe Paciente con ese ID");
        pacienteRepository.deleteById(id);
        return true;
    }


}
