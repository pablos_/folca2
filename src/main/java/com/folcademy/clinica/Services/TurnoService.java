package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService /*implements ITurnoService*/ {

    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    /*
    @Override
    public List<Turno> findAllTurnos() {
        return turnoRepository.findAll();
    }
    */

    public List<TurnoDto> findAllTurnos() {
        return  turnoRepository
                .findAll()
                .stream()
                .map(turnoMapper::entityToDto)
                .collect(Collectors.toList());
    }

    public TurnoDto findTurnoByID(Integer id){
        if(!turnoRepository.existsById(id)){
            throw new NotFoundException("No existe Turno con ese ID");
        }
        return turnoRepository
                .findById(id)
                .map(turnoMapper::entityToDto)
                .orElse(null);
    }

    public TurnoDto addNewTurno (TurnoDto entity){
        entity.setIdturno(null);

        if(entity.getIdpaciente()==null || entity.getIdmedico()==null )
            throw new RuntimeException("Paciente o Medico no pueden ser valor=Null");

        return turnoMapper.entityToDto((turnoRepository.save(turnoMapper.dtoToEntity(entity))));
    }

    public Boolean deleteTurno (Integer turnoID){
        if(!turnoRepository.existsById(turnoID))
            throw new NotFoundException("No existe ese ID de turno");

        turnoRepository.deleteById(turnoID);
        return true;

    }


}
