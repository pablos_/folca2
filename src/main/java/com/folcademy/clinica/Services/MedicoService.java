package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Exceptions.BadRequestException;
import com.folcademy.clinica.Model.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.lang.*;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService /*implements IMedicoService*/ {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    /*
    @Override
    public List<Medico> findAllMedicos() {
        return (List<Medico>) medicoRepository.findAll();
    }
    */

    public List<MedicoDto> listarTodos(){
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    /*
    @Override
    public List<Medico> findAllMedicos() {
        return (List<Medico>) medicoRepository.findAll();
    }
    */

    /*
    @Override
    public List<Medico> findMedicoById(Integer idmedico) {
        List<Medico> lista = new ArrayList<>();
        Medico medico = medicoRepository.findById(idmedico).get();
        lista.add(medico);
        return lista;
    }
    */

    public MedicoDto listarUno(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Ese ID no existe entre los Medicos");

        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }


    public MedicoDto agregar(MedicoDto entity){
        entity.setId(null);

        if(entity.getConsulta()<0){
            throw new BadRequestException("El precio de la consulta no puede ser menor a 0");
        }

        if(entity.getProfesion().isEmpty() || entity.getProfesion() == null){
            throw new ValidationException("No se ecuentra validado profesional este vacio");
        }
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw new RuntimeException("No existe medico con ese ID");

        dto.setId(idMedico);
        return medicoMapper.entityToEnteroDto(
                medicoRepository.save(
                        medicoMapper.enteroDtoToEntity(dto)
                )
        );
    }


    public boolean eliminar(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("Error Eliminacion. No existe Medico con ese ID");
            //return false;
        medicoRepository.deleteById(id);
        return true;
    }


}
